package com.oreillyauto.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "purchases")
public class Ticket implements Serializable {
    private static final long serialVersionUID = -4855603648459234422L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "tx_id", columnDefinition = "INTEGER")
    private Integer id;
    
    @Column(name = "room_id", columnDefinition = "INTEGER")
    private Integer roomId;
    
    @Column(name = "sale_date", columnDefinition = "DATE")
    private Date saleDate;
    
    @Column(name ="tendered", columnDefinition = "INTEGER")
    private Integer tendered;
    
    @Column(name ="change_given", columnDefinition = "INTEGER")
    private Integer change;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public Integer getTendered() {
        return tendered;
    }

    public void setTendered(Integer tendered) {
        this.tendered = tendered;
    }

    public Integer getChange() {
        return change;
    }

    public void setChange(Integer change) {
        this.change = change;
    }

}
