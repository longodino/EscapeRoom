package com.oreillyauto.domain.Need;

import java.util.Arrays;
import java.util.List;

import com.oreillyauto.domain.Ticket;

public class RestResponse {
    public RestResponse() {}
    private String message;
    private String[] messages;
    private Result result;
    private List<Ticket> ticketList;

    public String[] getMessages() {
        return messages;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public void setMessages(String[] messages) {
        this.messages = messages;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "RestResponse [messages=" + Arrays.toString(messages) + ", result=" + result + "]";
    }
    public void setTicketList(List<Ticket> tickets) {
        this.ticketList = tickets;
        
    }
}
