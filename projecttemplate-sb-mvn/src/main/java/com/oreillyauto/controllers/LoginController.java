package com.oreillyauto.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @GetMapping(value = {"/", "/login"})
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/home")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String home() {
        return "home";
    }
    
}
