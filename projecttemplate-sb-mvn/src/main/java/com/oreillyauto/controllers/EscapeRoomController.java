package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Ticket;
import com.oreillyauto.domain.Need.RestResponse;
import com.oreillyauto.service.EscapeRoomService;
import com.oreillyauto.util.Helper;

@Controller
public class EscapeRoomController {
    
    @Autowired
    EscapeRoomService esService;
    
    @GetMapping( value = {"/index"})
    public String getHomePage(Model model) {
        model.addAttribute("ticketList", esService.getTickets());
        
        List<Ticket> ticketList = esService.getTickets();
        try {
            model.addAttribute("ticketListJson", Helper.getJson(ticketList));
        }
        catch(Exception e) {
            System.out.println(e.getStackTrace());
        }
        
        return "index";
    }
    
    @ResponseBody
    @PostMapping(value = { "/addTicket" })
    public RestResponse postDeleteUser(@RequestBody Ticket ticket) {
        RestResponse response = new RestResponse();
        
        try {
            if (ticket != null) {
                esService.addticket(ticket);
                response.setMessage("Student Successfully Enrolled");

                response.setTicketList(esService.getTickets());
            } else {
                response.setMessage("Sorry, unable to add the student.");
            }
            
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setMessage(Helper.getError(e));
            return response;
        }
    }

}
