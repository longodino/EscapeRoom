package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.EscapeRoomRepositoryCustom;
import com.oreillyauto.domain.Ticket;

public interface EscapeRoomRepository extends CrudRepository<Ticket, Integer>, EscapeRoomRepositoryCustom {

}
