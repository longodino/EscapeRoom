package com.oreillyauto.dao.impl;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.EscapeRoomRepositoryCustom;
import com.oreillyauto.domain.Ticket;

@Repository
public class EscapeRoomRepositoryImpl  extends QuerydslRepositorySupport implements EscapeRoomRepositoryCustom{

    public EscapeRoomRepositoryImpl() {
        super(Ticket.class);
    }
    
    
}
