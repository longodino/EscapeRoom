package com.oreillyauto.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.EscapeRoomRepository;
import com.oreillyauto.domain.Ticket;
import com.oreillyauto.service.EscapeRoomService;

@Service("escapeRoomService")
public class EscapeRoomServiceImpl implements EscapeRoomService {
    @Autowired
    EscapeRoomRepository esrepo;

    @Override
    public List<Ticket> getTickets() {
        return (List<Ticket>) esrepo.findAll();
    }

    @Override
    public void addticket(Ticket ticket) {
        esrepo.save(ticket);
    }

}
