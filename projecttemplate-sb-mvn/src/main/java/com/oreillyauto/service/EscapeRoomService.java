package com.oreillyauto.service;

import java.util.List;

import com.oreillyauto.domain.Ticket;

public interface EscapeRoomService {
    public List<Ticket> getTickets();

    public void addticket(Ticket ticket);
}
