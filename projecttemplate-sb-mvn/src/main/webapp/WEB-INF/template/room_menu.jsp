<%@ include file="/WEB-INF/layouts/include.jsp"%>

<nav class="navbar bg-light">
	<!-- Links -->
	<!-- <ul class="navbar-nav"> -->
	<ul class="nav nav-pills flex-column">
		<li>
		    <a class="${active eq 'new_sell' ? 'active nav-link' : 'nav-link'}" 
		       href="<c:url value='/newsell' />">
		    	New Sell
		    </a>
		</li>
		<li class="nav-item">
			<a class="${active eq 'report' ? 'active nav-link' : 'nav-link'}" 
			   href="<c:url value='/report'/>">
				Escape Room Report
			</a>
		</li>
	</ul>
</nav>