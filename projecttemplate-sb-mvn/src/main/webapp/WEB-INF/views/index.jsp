<%@ include file="/WEB-INF/layouts/include.jsp" %>

<h1>No Way Out INC</h1>

<div class="row">
	<div class="col-sm-12">
		<form is="orly-form" id="form" action="<c:url value='/addTicket' />">
			<div class ="row">
				<div class="col-sm-2">
	    			<label for="roomName">Room Name</label>
	    		</div>
		    	<div class="col-sm-8 form-group">
		    	
				    <orly-input id="roomName" name="roomName" value="${ticket.roomName}" required placeholder="Room Name">
				      <orly-option label="Alcatraz" value="1"></orly-option>
				      <orly-option label="Missouri" value="2"></orly-option>
				    </orly-input>
				 </div>
			</div>
			<div class ="row">
				<div class="col-sm-2">
					<label for="saleDate" class="mt10">Enroll Date</label>
				</div>
				<div class="col-sm-8 form-group">
					
					<orly-datepicker id="saleDate" name="saleDate" 
					                 value="${ticket.saleDate }" size="sm"></orly-datepicker>
				</div>
			</div>
			<div class ="row">
				<div class="col-sm-2">
					<label for="tendered">Tendered</label>
				</div>			
				<div class="col-sm-8 form-group">
				  <orly-input id="tendered" name="tendered" value="${ticket.tendered}"  placeholder="Tendered"></orly-input>
				</div>
			</div>
			<div class ="row">
				<div class="col-sm-2">
					<label for="change">Change</label>
				</div>			
				<div class="col-sm-4 form-group">
				  <orly-input id="change" name="change" value="${ticket.change}"  placeholder="Change"></orly-input>
				</div>
			</div>
			<div class="col-sm-4 form-group">
			  <!-- <orly-btn id="submitBtn" type="submit" spinonclick text="Submit"></orly-btn> -->
			  <button type="submit" id="submitBtn" class="btn btn-primary">Submit</button>
			</div>
		</form>
		
		
		<hr/>

		<h2>Tickets</h2>
		<c:set value="${fn:length(ticketListJson) gt 0 ? ticketListJson : []}" var="tableData"/>
		<orly-table id="ticketTable" loaddataoncreate includefilter bordered maxrows="10" tabletitle="Search Results" class="invisible" data='${tableData}'>
			<orly-column field="id" label="Ticket Id" class="" sorttype="natural"></orly-column>
			<orly-column field="roomId" label="Room Id"></orly-column>
			<orly-column field="saleDate" label="Sale Date"></orly-column>
			<orly-column field="tendered" label="Tendered"></orly-column>
			<orly-column field="change" label="Change"></orly-column>
		</orly-table>
		
<script>	
	var ticketTable;

	orly.ready.then(() => {
		ticketTable = orly.qid("ticketTable");
		
		ticketTable.updateColumn("saleDate", "cellFn", function(row, cell, item) { 			
			return orly.formatDate(new Date(item.enrollDate), 'MM/DD/YYYY');
		});
		
		orly.qid("submitBtn").addEventListener("click", function(e){
			try {
				e.preventDefault(); // Prevent Form From Submitting "normally" (IE)
				let Ticket = {};
				Ticket.roomName = orly.qid("roomName").value;
				if(Ticket.roomName == "Alactraz"){
					Ticket.roomId = 1;
				}
				else{
					Ticket.roomId = 2;
				}
				
				Ticket.saleDate = orly.qid("saleDate").value;
				Ticket.tendered = orly.qid("tendered").value;
				Ticket.change = orly.qid("change").value;
				
				// Make AJAX call to the server
				fetch("<c:url value='/addTicket' />", {
				        method: "POST",
				        body: JSON.stringify(Ticket),
				        headers: {
				            "Content-Type": "application/json"
				        }
				}).then(function(response) {
				  	if (response.ok) {
				  		console.log("response.status=", response.status);
				  		let message = response.json();
				  		console.log("message = " + message);
				  		return message
				  	} else {
				  		throw new Error("Error: " + response.statusText);
				  	}
				}).then(function(Response) {
					//let Message = JSON.parse(response);
					let message = Response.message;
					let messageType = Response.messageType;
					
					if (messageType == null || messageType == "undefined" || messageType.length == 0) {
						messageType = "info";
					}
				  	
				  	if (message != null && message != "undefined" && message.length > 0) {
				  		orly.qid("alerts").createAlert({type:messageType, duration:"3000", msg:message});	
				  	}
				  	
				  	populateTableWithResponse(Response);
				  	
				}).catch(function(error) {
					let message = 'There was a problem with your fetch operation: ' + error.message;
					orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:message});
				});
		
			} catch (err) {
				// Do not show try-catch errors to the user in production
				orly.qid("alerts").createAlert({type:"danger", duration:"3000", msg:err});
			}
		});
	});
	
	function populateTableWithResponse(Response) {
		try {
			let ticketList = Response.ticketList;
			
			if (ticketList != null && ticketList != "undefined") {
				ticketTable.data = ticketList;
			} else {
				console.log("Unable to populate table with new ticket list");
			}	
		} catch (e) {
			console.log("Unable to populate table with new ticket list");	
		}
	}
</script>	
		
		
